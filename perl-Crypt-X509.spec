Name:                perl-Crypt-X509
Version:             0.55
Release:             2
Summary:             Parse a X.509 certificate
License:             GPL-1.0-or-later OR Artistic-1.0-Perl
URL:                 https://metacpan.org/release/Crypt-X509
Source0:             https://cpan.metacpan.org/authors/id/A/AJ/AJUNG/Crypt-X509-%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       coreutils findutils make perl-interpreter perl-generators perl(Carp)
BuildRequires:       perl(Convert::ASN1) >= 0.19 perl(Exporter) perl(ExtUtils::MakeMaker)
BuildRequires:       perl(Math::BigInt) perl(Test::More) perl(strict) perl(warnings)
Requires:            perl(Convert::ASN1) >= 0.19
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Convert::ASN1\\)
%description
Crypt::X509 parses X.509 certificates. Methods are provided for accessing
most certificate elements.

%package help
Summary:             Help files for perl-Crypt-X509
Requires:            %{name} = %{version}-%{release}

%description help
This package provides help files forperl-Crypt-X509

%prep
%setup -q -n Crypt-X509-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*


%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.55-2
- cleanup spec

* Thu Jul 13 2023 leeffo <liweiganga@uniontech.com> - 0.55-1
- upgrade to version 0.55

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.54-1
- Upgrade to version 0.54

* Sat Feb 20 2021 sunguoshuai <sunguoshuai@huawei.com> - 0.51-1
- package init
